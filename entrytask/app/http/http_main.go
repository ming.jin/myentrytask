package main

import (
	"encoding/gob"
	"entrytask/common"
	"entrytask/protocal"
	"entrytask/rpc"
	"entrytask/service"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	// 给gob注册类型
	gob.Register(protocal.LoginReq{})
	gob.Register(protocal.LoginRes{})
	gob.Register(protocal.ShowInfoReq{})
	gob.Register(protocal.ShowInfoRes{})
	gob.Register(protocal.AlterNickReq{})
	gob.Register(protocal.AlterNickRes{})
	gob.Register(protocal.AlterPicReq{})
	gob.Register(protocal.AlterpicRes{})
	gob.Register(protocal.LogoutReq{})
	gob.Register(protocal.User{})
	service.RpcClient = rpc.NewClient("localhost:8081", 2000, service.RpcConnPoolSize)
}

//func waitElegantExit(c chan os.Signal) {
//	for i := range c {
//		switch i {
//		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
//			// 这里做一些清理操作或者输出相关说明，比如 断开数据库连接
//			dao2.MysqlClose()
//			dao2.RedisClose()
//			fmt.Println("receive exit signal ", i.String(), ",exit...")
//			os.Exit(0)
//		}
//	}
//}

func main() {
	//	http.Handle("/pic/", http.StripPrefix("/pic/", http.FileServer(http.Dir("./src/web/pic"))))
	http.Handle("/web/", http.StripPrefix("/web/", http.FileServer(http.Dir("./web"))))
	http.HandleFunc("/login/", service.LoginHandler)
	http.HandleFunc("/userinfo/", service.GetInfoHandler)
	http.HandleFunc("/alternickname/", service.AlterNickHandler)
	http.HandleFunc("/alterpic/", service.AlterPicHandler)
	http.HandleFunc("/logout/", service.LogoutHandler)
	fmt.Println("Starting HttpServer at http://127.0.0.1:8080")
	http.ListenAndServe(":8080", nil)

	c := make(chan os.Signal)
	// SIGHUP: terminal closed
	// SIGINT: Ctrl+C
	// SIGTERM: program exit
	// SIGQUIT: Ctrl+/
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// 阻塞，直到接受到退出信号，才停止进程
	common.WaitElegantExit(c)
}
