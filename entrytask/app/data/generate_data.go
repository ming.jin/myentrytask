package main

import (
	"entrytask/dao"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
)

const batch_size = 1000

func main() {
	SqlInitErr := dao.InitDB()
	if SqlInitErr != nil {
		return
	}
	fmt.Printf("begin insert\n")

	sqlStr := "insert into user (username, password, nickname, picture) values "
	var dataStr string = sqlStr
	for i := 1; i <= 10000000; i++ {
		name := "username" + strconv.Itoa(i)
		nick := "nickname" + strconv.Itoa(i)
		dataStr += "('" + name + "', '123456' ,'" + nick + "', 'pic/default.png')"

		if i%batch_size == 0 {
			dataStr += ";"
			fmt.Println(dataStr)
			_, err := dao.Db.Exec(dataStr)
			if err != nil {
				fmt.Println("insert %s failed, error: %s\n", name, err.Error())
				return
			}
			dataStr = sqlStr
		} else {
			dataStr += ","
		}

	}

	//sqlStr := "insert into test (username, password, nickname, picture) values ( ?, ?, ?, ?)"
	//stmt, err := dao.Db.Prepare(sqlStr)
	//fmt.Println("come here~")
	//if err != nil {
	//	fmt.Printf("prepare failed, err:%v\n", err)
	//	return
	//}
	//fmt.Println("come here!")
	//for i := 2; i <= 10000000; i++ {
	//	name := "username" + strconv.Itoa(i)
	//	nick := "nickname" + strconv.Itoa(i)
	//	fmt.Println(name, "____________", nick)
	//	_, err := stmt.Exec(name, "123456", nick, "pic/default.png")
	//	if err != nil {
	//		fmt.Printf("insert failed, err:%v\n", err)
	//		return
	//	}
	//}

}
