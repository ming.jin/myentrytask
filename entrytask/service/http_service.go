package service

import (
	"entrytask/common"
	"entrytask/protocal"
	"entrytask/rpc"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

// 客户端与服务器连接池的大小
const RpcConnPoolSize int = 3000

var RpcClient *rpc.Client

// 初始化
//func init() {
//	//创建客户端
//	RpcClient = rpc.NewClient("localhost:8081", 2000, RpcConnPoolSize)
//}

func Close() {
	RpcClient.Pool.Close()
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.FormValue("username")

	password := r.FormValue("password")
	fmt.Println("LoginHandler--->", username, "~~~~~~", password)

	//rpc 定义函数调用原型
	var Login func(protocal.LoginReq) (protocal.LoginRes, string, int)
	RpcClient.CallRPC("Login", &Login)
	response, msg, code := Login(protocal.LoginReq{Username: username, Password: password})
	fmt.Println("resp in LoginHandler", response)

	//在服务端拿到Token，存在cookie中 用于鉴权
	codeCookie := &http.Cookie{
		Name:     "token",
		Value:    response.ResToken,
		Path:     "/",
		Secure:   false,
		HttpOnly: false,
		MaxAge:   int(1 * 60 * 60 * time.Second),
	}
	http.SetCookie(w, codeCookie)
	common.JsonResp(w, response, msg, code)
	//w.Header().Set("Content-type", "application/json")
	//w.Header().Set("Access-Control-Allow-Origin", "*")
	//respMap := map[string]interface{}{"data": response, "msg": msg, "code": code}
	//respJson, err := json.Marshal(respMap)
	//if err != nil {
	//	log.Println("WriteJson Err:", err)
	//	return
	//}
	//fmt.Println(string(respJson))
	//w.Write(respJson)
}

func GetInfoHandler(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	token := r.FormValue("token")
	fmt.Println("GetInfoHandler--->", username, "~~~~~~", token)

	//rpc 定义函数调用原型
	var GetUserInfo func(protocal.ShowInfoReq) (protocal.ShowInfoRes, string, int)
	RpcClient.CallRPC("GetInfo", &GetUserInfo)
	response, msg, code := GetUserInfo(protocal.ShowInfoReq{Token: token, Username: username})
	fmt.Println("resp in GetInfoHandler", response)
	common.JsonResp(w, response, msg, code)

	//cookieCode, err := r.Cookie("token")
	//if err != nil {
	//	str := "暂无cookie信息"
	//	co := common.NoCookieError
	//	fmt.Println("get token error:", err)
	//	common.ErrJsonResp(w, co, str)
	//}
	//w.Header().Set("Content-type", "application/json")
	//w.Header().Set("Access-Control-Allow-Origin", "*")
	//respMap := map[string]interface{}{"data": "", "msg": "暂无cookie信息", "code": common.NoCookieError}
	//respJson, err := json.Marshal(respMap)
	//if err != nil {
	//	log.Println("WriteJson Err:", err)
	//	return
	//}
	//w.Write(respJson)
	//var token string = cookieCode.Value

}

func AlterNickHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.FormValue("username")
	nickname := r.FormValue("nickname")
	token := r.FormValue("token")
	fmt.Println("AlterNickHandler--->", username, "~~~~~~", nickname, "~~~~~~", token)

	//rpc 定义函数调用原型
	var AlterNickname func(req protocal.AlterNickReq) (protocal.AlterNickRes, string, int)
	RpcClient.CallRPC("AlterNick", &AlterNickname)
	response, msg, code := AlterNickname(protocal.AlterNickReq{Token: token, Username: username, NewNickname: nickname})
	fmt.Println("resp in AlterNickHandler", response)
	common.JsonResp(w, response, msg, code)
}

//检查目录是否存在
func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Printf("%s does not exist\n", filename)
		exist = false
	}
	return exist
}

func AlterPicHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(1024)
	username := r.FormValue("username")
	token := r.FormValue("imagetoken")
	picture, picHeader, err := r.FormFile("image")
	//	fmt.Println("AlterPicHandler--->", username, "~~~~~~", token, "~~~~~~", picture)
	if err != nil {
		fmt.Println("upload picture failed, error: ", err)
		//common.ErrJsonResp(w, common.UpdatePicError, "更新头像失败")
		return
	}

	path_exist := checkFileIsExist("./web/pic")
	// 目录不存在，创建
	if !path_exist {
		mkdirErr := os.Mkdir("./web/pic", 0777)
		if mkdirErr != nil {
			fmt.Printf("mkdir pic failed, error: %s\n", mkdirErr.Error())
			//		common.ErrJsonResp(w, common.UpdatePicError, "更新头像失败")
			return
		}
	}
	// 存储路径
	newPic := "pic/" + picHeader.Filename
	fmt.Println("AlterPicHandler--->", username, "~~~~~~", token, "~~~~~~", newPic)
	////rpc 定义函数调用原型
	var AlterPic func(req protocal.AlterPicReq) (protocal.AlterpicRes, string, int)
	RpcClient.CallRPC("AlterPic", &AlterPic)
	response, msg, code := AlterPic(protocal.AlterPicReq{Token: token, Username: username, Newpic: newPic})
	fmt.Println("resp in AlterPicHandler", response, msg, code)

	if code != 1000 {
		return
	}
	// 保存图片
	localPath := "./web/" + newPic
	out, err := os.Create(localPath)
	if err != nil {
		fmt.Println("failed to open the picture for writing,", newPic)
		//	common.ErrJsonResp(w, common.UpdatePicError, "更新头像失败")
		return
	}
	defer out.Close()

	_, err = io.Copy(out, picture)
	if err != nil {
		fmt.Println("copy pictue error:", err)
		//	common.ErrJsonResp(w, common.UpdatePicError, "更新头像失败")
		return
	}
	//	common.JsonResp(w, response, msg, code)
	fmt.Println("OK here~~~~~~~~~~~~~~~~~~~~~~~~~~")
	http.Redirect(w, r, "http://127.0.0.1:8080/web/usercenter.html", 302)
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.FormValue("username")
	token := r.FormValue("token")
	fmt.Println("LogoutHandler--->", username, "~~~~~~", token)

	//rpc 定义函数调用原型
	var Logout func(req protocal.LogoutReq) (string, int)
	RpcClient.CallRPC("Logout", &Logout)
	msg, code := Logout(protocal.LogoutReq{Token: token, Username: username})
	common.ErrJsonResp(w, code, msg)
}
