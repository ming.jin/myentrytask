package service

import (
	"database/sql"
	"entrytask/common"
	"entrytask/dao"
	"entrytask/protocal"
	"fmt"
)

func Login(req protocal.LoginReq) (resp protocal.LoginRes, msg string, code int) {
	//从redis查找
	fmt.Println("Login--->", req.Username, "~~~~~~", req.Password)
	//resp = new(protocal.LoginRes)
	resp.Username = req.Username
	flag := false
	userFromRedis, err := dao.GetUser(req.Username)
	//if err != nil || userFromRedis == nil{
	//	code = common.RedisOpError
	//	fmt.Println("get info from redis failed")
	//}

	if err == nil && userFromRedis != (protocal.User{}) {
		if userFromRedis.Password == req.Password {
			err = dao.SetUser(userFromRedis)
			flag = true
			fmt.Println("get info from redis success")

			////从reids读token
			//token, err := dao.GetToken(req.Username)
			////if err != nil {
			////	resp.Code = common.RedisOpError
			////}
			//if err == nil {
			//	fmt.Println("get token from redis success")
			//	resp.ResToken = token
			//	code = common.Correct
			//	msg = "登录成功"
			//	return resp, msg, code
			//}

		} else {
			code = common.PwdError
			msg = "密码错误"
			fmt.Println("pwd not right")
			return resp, msg, code
		}
	}

	//从mysql查找
	if !flag {
		userFromSql, err := dao.FindUserByName(req.Username)
		fmt.Println("Login   user from mysql--->", userFromSql)
		if err != nil {
			if err == sql.ErrNoRows {
				code = common.NoUserError
				msg = "无此用户"
				fmt.Println("don't get the user from mysql")
			} else {
				code = common.MysqlOpError
				msg = "mysql操作失败"
				fmt.Println("get the user from mysql failed")
			}
			return resp, msg, code
		}
		if userFromSql.Password != req.Password {
			code = common.PwdError
			fmt.Println("pwd not right")
			msg = "密码错误"
			return resp, msg, code
		}
		err = dao.SetUser(userFromSql)
		if err != nil {
			//	code = common.RedisOpError
			fmt.Println("save the user into redis error")
		}
	}

	//存入redis
	token, err := dao.SetToken(req.Username)
	if err != nil {
		code = common.SaveTokenError
		msg = "token存储失败"
		fmt.Println("save token into redis error")
	} else {
		resp.ResToken = token
		code = common.Correct
		msg = "登录成功"
		fmt.Println("save token into redis success")
	}
	return resp, msg, code
}

func Auth(username string, tokenpara string) (msg string, code int) {
	if len(username) <= 0 {
		msg = "缺少用户名信息，鉴权失败"
		code = common.LockParaError
		return msg, code
	} else {
		token, err := dao.GetToken(username)
		if err != nil || len(token) <= 0 || token != tokenpara {
			msg = "鉴权失败"
			code = common.AuthError
			fmt.Println("auth error")
		} else {
			msg = "鉴权成功"
			//	fmt.Println("auth success:", token, tokenpara)
			code = common.AuthSuccess
		}
		return msg, code
	}
}

func GetUserInfo(req protocal.ShowInfoReq) (resp protocal.ShowInfoRes, msg string, code int) {
	//resp = new(protocal.ShowInfoRes)
	//鉴权
	msg, code = Auth(req.Username, req.Token)
	if code != common.AuthSuccess {
		return resp, msg, code
	}

	//从redis查找
	userFromRedis, err := dao.GetUser(req.Username)
	if err == nil && userFromRedis != (protocal.User{}) {
		resp.UserInfo.Uid = userFromRedis.Uid
		resp.UserInfo.Username = userFromRedis.Username
		resp.UserInfo.Password = userFromRedis.Password
		resp.UserInfo.Nickname = userFromRedis.Nickname
		resp.UserInfo.Picture = "web/" + userFromRedis.Picture
		msg = "成功获取用户信息"
		code = common.Correct
		fmt.Println("get info from redis success")
		return resp, msg, code
	}

	//从mysql查找
	userFromSql, err := dao.FindUserByName(req.Username)
	if err != nil {
		if err == sql.ErrNoRows {
			code = common.NoUserError
			msg = "无此用户"
			fmt.Println("don't get the user from mysql")
		} else {
			code = common.MysqlOpError
			msg = "mysql操作失败"
			fmt.Println("get the user from mysql failed")
		}
		return resp, msg, code
	}
	resp.UserInfo.Uid = userFromSql.Uid
	resp.UserInfo.Username = userFromSql.Username
	resp.UserInfo.Password = userFromSql.Password
	resp.UserInfo.Nickname = userFromSql.Nickname
	resp.UserInfo.Picture = "web/" + userFromSql.Picture
	fmt.Println("get the user from mysql success")

	//用户信息存入redis
	err = dao.SetUser(userFromSql)
	if err != nil {
		fmt.Println("save the user into redis error")
	}

	code = common.Correct
	msg = "成功获取用户信息"
	return resp, msg, code
}

func AlterNickname(req protocal.AlterNickReq) (resp protocal.AlterNickRes, msg string, code int) {
	//鉴权
	msg, code = Auth(req.Username, req.Token)
	if code != common.AuthSuccess {
		return resp, msg, code
	}

	flag := false
	//从redis查找
	userFromRedis, _ := dao.GetUser(req.Username)
	if userFromRedis != (protocal.User{}) {
		if userFromRedis.Nickname == req.NewNickname {
			msg = "新昵称不能与旧昵称相同，修改昵称失败"
			code = common.NickSameError
			return resp, msg, code
		} else {
			//不用从mysql里找
			flag = true
		}
	}
	if flag == false {
		userFromMysql, err := dao.FindUserByName(req.Username)
		if err != nil {
			msg = "mysql操作失败，修改昵称失败"
			code = common.MysqlOpError
			return resp, msg, code
		}
		if userFromMysql != (protocal.User{}) {
			if userFromMysql.Nickname == req.NewNickname {
				msg = "新昵称不能与旧昵称相同"
				code = common.NickSameError
				return resp, msg, code
			}
		} else {
			msg = "无此用户信息"
			code = common.NoUserError
			return resp, msg, code
		}
	}

	//删除redis中的用户信息
	dao.DelUser(req.Username)
	user, err := dao.UpdateNick(req.Username, req.NewNickname)
	if err != nil || user == (protocal.User{}) {
		fmt.Println("upate nickname failed")
		msg = "修改昵称失败"
		code = common.UpdateNickError
	} else {
		resp.UserInfo.Uid = user.Uid
		resp.UserInfo.Username = user.Username
		resp.UserInfo.Password = user.Password
		resp.UserInfo.Nickname = user.Nickname
		resp.UserInfo.Picture = "web/" + user.Picture
		fmt.Println("upate nickname success")
		msg = "修改昵称成功"
		code = common.Correct
	}
	return resp, msg, code
}

func AlterPic(req protocal.AlterPicReq) (resp protocal.AlterpicRes, msg string, code int) {
	//鉴权
	msg, code = Auth(req.Username, req.Token)
	if code != common.AuthSuccess {
		return resp, msg, code
	}
	//删除redis中的用户信息
	dao.DelUser(req.Username)
	user, err := dao.UpdatePic(req.Username, req.Newpic)
	if err != nil || user == (protocal.User{}) {
		fmt.Println("upate picture failed")
		msg = "更新头像失败"
		code = common.UpdatePicError
	} else {
		resp.UserInfo.Uid = user.Uid
		resp.UserInfo.Username = user.Username
		resp.UserInfo.Password = user.Password
		resp.UserInfo.Nickname = user.Nickname
		resp.UserInfo.Picture = "web/" + user.Picture
		fmt.Println("upate picture success")
		msg = "更新头像成功"
		code = common.Correct
	}
	return resp, msg, code
}

func Logout(req protocal.LogoutReq) (msg string, code int) {
	//鉴权
	msg, code = Auth(req.Username, req.Token)
	if code != common.AuthSuccess {
		return msg, code
	}
	//删除redis中的用户信息
	//	dao.DelUser(req.Username)
	//删除redis中的token信息
	err := dao.DeleteToken(req.Username)
	if err != nil {
		msg = "删除token失败"
		code = common.DeleteTokenError
	} else {
		msg = "删除token成功"
		code = common.Correct
	}
	return msg, code
}
