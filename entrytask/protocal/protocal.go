package protocal

type User struct {
	Uid      int
	Username string
	Password string
	Nickname string
	Picture  string
}

//login
type LoginReq struct {
	Username string
	Password string
}

type LoginRes struct {
	//	ResCode int `json:"ResCode"`
	//	Msg      string  `json:"Msg"`
	ResToken string `json:"ResToken"`
	Username string `json:"Username"`
}

//showInfo
type ShowInfoReq struct {
	Token    string
	Username string
}

type ShowInfoRes struct {
	//	Code int
	//	Msg      string
	UserInfo User
}

//alterNick
type AlterNickReq struct {
	Token       string
	Username    string
	NewNickname string
}

type AlterNickRes struct {
	//Code int
	//	Msg  string
	UserInfo User
}

//alterPic
type AlterPicReq struct {
	Token    string
	Username string
	Newpic   string
}

type AlterpicRes struct {
	//  Code int
	//	Msg  string
	UserInfo User
}

type LogoutReq struct {
	Token    string
	Username string
}
