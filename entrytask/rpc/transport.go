package rpc

import (
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

// 通过TLV协议传输数据

type Transport struct {
	conn net.Conn // 网络连接
}

// 创建一个传输对象
func NewTransport(conn net.Conn) *Transport {
	return &Transport{conn}
}

// 发送数据
func (t *Transport) Send(data []byte) error {
	// 数据格式：数据长度（4字节）+ 数据内容
	buf := make([]byte, 4+len(data))
	binary.BigEndian.PutUint32(buf[:4], uint32(len(data)))
	copy(buf[4:], data)
	// 向连接写入数据
	_, err := t.conn.Write(buf)
	if err != nil {
		fmt.Println("Send data through connection failed, error: %v\n", err)
		return err
	}
	return nil
}

// 从连接连接中接受并读取数据
func (t *Transport) Receive() (data []byte, err error) {
	// 读取数据长度
	header := make([]byte, 4)
	_, err = t.conn.Read(header)
	if err != nil {
		fmt.Println("Read length of data from connection failed, error: %v\n", err)
		return
	}
	// 读取数据内容
	dataLen := binary.BigEndian.Uint32(header)
	data = make([]byte, dataLen)
	_, err = io.ReadFull(t.conn, data)
	if err != nil {
		fmt.Println("Read data from connection failed, error: %v\n", err)
		return
	}
	return
}
