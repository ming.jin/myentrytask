package rpc

import (
	"fmt"
	"net"
	"reflect"
)

// RPCServer
type RPCServer struct {
	addr  string
	funcs map[string]reflect.Value
}

// 远程调用注册
func (s *RPCServer) Register(fnName string, fFunc interface{}) {
	if _, ok := s.funcs[fnName]; ok {
		return
	}
	s.funcs[fnName] = reflect.ValueOf(fFunc)
}

// 创建rpc server
func NewRPCServer(addr string) *RPCServer {
	return &RPCServer{addr: addr, funcs: make(map[string]reflect.Value)}
}

func (s *RPCServer) Run() {
	listener, err := net.Listen("tcp", s.addr)
	if err != nil {
		fmt.Println("Listen error: %v\n", err.Error())
		return
	}
	defer listener.Close()

	for {
		// 监听连接
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Accept error: %v\n", err.Error())
			continue
		}
		go Process(conn, s)
	}
}

func Process(conn net.Conn, ser *RPCServer) {
	for {
		srvtp := NewTransport(conn)

		// 读取数据
		reqdata, err := srvtp.Receive()
		if err != nil {
			fmt.Println("Receive error: %v\n", err)
			return
		}

		// 解析数据
		req, err := Deserialize(reqdata)
		if err != nil {
			fmt.Println("Deserialize error: %v\n", err)
			return
		}

		// 调用函数
		rspdata := ser.Execute(req)
		if rspdata.Args == nil {
			return
		}

		// 返回数据进行编码
		data, err := Serialize(rspdata)
		if err != nil {
			fmt.Println("Serialize error: %v\n", err)
			return
		}

		// 回应
		senderr := srvtp.Send(data)
		if senderr != nil {
			fmt.Println("Send error: %v\n", senderr)
			return
		}
	}
}

func (s *RPCServer) Execute(req RPCdata) RPCdata {
	// 查找函数
	f, ok := s.funcs[req.Name]
	if !ok {
		e := fmt.Sprintf("func %s not Registered", req.Name)
		fmt.Println("func %s not Registered", e)
		return RPCdata{Name: req.Name, Args: nil}
	}

	// 解析请求参数
	inArgs := make([]reflect.Value, len(req.Args))
	for i := range req.Args {
		inArgs[i] = reflect.ValueOf(req.Args[i])
	}

	// 调用制定的函数
	out := f.Call(inArgs)
	// 处理函数调用的输出
	resArgs := make([]interface{}, len(out))
	for i := 0; i < len(out); i++ {
		resArgs[i] = out[i].Interface()
	}

	//// 处理error参数
	//var er string
	//if e, ok := out[len(out)-1].Interface().(error); ok {
	//	// 转成string
	//	er = e.Error()
	//}

	return RPCdata{Name: req.Name, Args: resArgs}
}
