package rpc

import (
	"fmt"
	"net"
	"reflect"

	"github.com/fatih/pool"
)

type Client struct {
	Pool pool.Pool
}

// 创建一个新的client
func NewClient(servAddr string, idelConns int, maxConns int) *Client {
	factory := func() (net.Conn, error) {
		return net.Dial("tcp", servAddr)
	}

	p, err := pool.NewChannelPool(idelConns, maxConns, factory)
	if err != nil {
		fmt.Println("Create connection pool for client failed, error: %s", err.Error())
		return nil
	}
	return &Client{p}
}

func (c *Client) CallRPC(rpcName string, fnPtr interface{}) {
	// 获取函数的反射
	reflection := reflect.ValueOf(fnPtr).Elem()

	fn := func(req []reflect.Value) []reflect.Value {

		errorHandler := func(err error) []reflect.Value {
			outArgs := make([]reflect.Value, reflection.Type().NumOut())
			for i := 0; i < len(outArgs); i++ {
				outArgs[i] = reflect.Zero(reflection.Type().Out(i)) //返回对应类型的零值
			}
			return outArgs
		}

		// 从连接池中取一个连接
		conn, err := c.Pool.Get()
		fmt.Println("pool err:", err)
		defer conn.Close()

		cReqTransport := NewTransport(conn)

		// 处理函数的入参
		inArgs := make([]interface{}, 0, len(req))
		for _, arg := range req {
			inArgs = append(inArgs, arg.Interface())
		}
		// 对rpc数据编码
		reqRPC := RPCdata{Name: rpcName, Args: inArgs}
		b, err := Serialize(reqRPC)
		if err != nil {
			fmt.Println("call prc failed in encoding data, function name: %v, error: %v\n", rpcName, err)
			return nil
		}

		// 向服务器发送rpc请求
		err = cReqTransport.Send(b)
		if err != nil {
			fmt.Println("call prc failed in sending request, function name: %v, error: %v\n", rpcName, err)
			return errorHandler(err)
		}

		// 读取response数据
		rsp, err := cReqTransport.Receive()
		if err != nil {
			fmt.Println("call prc failed in reading response, function name: %v, error: %v\n", rpcName, err)
			return errorHandler(err)
		}

		// 解析repsonse数据
		rspDecode, err := Deserialize(rsp)
		if err != nil {
			fmt.Println("call prc failed in decoding response, function name: %v, error: %v\n", rpcName, err)
			return errorHandler(err)
		}

		if len(rspDecode.Args) == 0 {
			rspDecode.Args = make([]interface{}, reflection.Type().NumOut())
		}

		// 解析参数
		numOut := reflection.Type().NumOut()
		outArgs := make([]reflect.Value, numOut)
		for i := 0; i < numOut; i++ {
			// 如果某个argument为nil，则将它设置为对应类型的"Zero"值
			if rspDecode.Args[i] == nil {
				outArgs[i] = reflect.Zero(reflection.Type().Out(i))
			} else {
				outArgs[i] = reflect.ValueOf(rspDecode.Args[i])
			}
		}

		return outArgs
	}

	reflection.Set(reflect.MakeFunc(reflection.Type(), fn))
}
