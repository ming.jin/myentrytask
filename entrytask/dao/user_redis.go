package dao

import (
	"crypto/md5"
	"encoding/json"
	"entrytask/protocal"
	"fmt"
	"github.com/go-redis/redis"
	"strconv"
	"time"
)

var RClient *redis.Client

// 初始化连接
func InitRedis() (err error) {
	// 建立连接池
	RClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,

		//连接池容量及闲置连接数量
		PoolSize:     1000, // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
		MinIdleConns: 20,   //在启动阶段创建指定数量的Idle连接，并长期维持idle状态的连接数不少于指定数量；。

	})
	_, err = RClient.Ping().Result()
	if err != nil {
		fmt.Printf("redis init error:", err)
		return err
	}
	fmt.Println("Redis connection success")
	return nil
}

func RedisClose() {
	RClient.Close()
}

func GetUser(username string) (user protocal.User, err error) {
	val, err := RClient.Get(username + "info").Result()
	if err != nil {
		return user, err
	}

	//var user protocal.User
	err1 := json.Unmarshal([]byte(val), &user)
	if err1 != nil {
		return user, err
	}
	return user, nil
}

func SetUser(user protocal.User) (err error) {
	jsonUser, _ := json.Marshal(user)
	_, err = RClient.SetNX(user.Username+"info", string(jsonUser), time.Minute*30).Result()
	return err
}

func DelUser(username string) (err error) {
	_, err = RClient.Del(username + "info").Result()
	return err
}

func SetToken(username string) (token string, err error) {
	//ctx := context.Background()
	srcstr := strconv.FormatInt(time.Now().UnixNano(), 10) + username
	h := md5.New()
	h.Write([]byte(srcstr))
	token = fmt.Sprintf("%x", h.Sum(nil))
	//token = "testtokeninfo"
	_, err = RClient.Set(username+"token", token, time.Minute*10).Result()
	if err != nil {
		return "", err
	}
	return token, nil
}

func GetToken(username string) (token string, err error) {
	//查看缓存
	return RClient.Get(username + "token").Result()
}

func DeleteToken(userName string) (err error) {
	_, err = RClient.Del(userName + "token").Result()
	return err
}
