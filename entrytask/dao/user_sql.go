package dao

import (
	"database/sql"
	"entrytask/protocal"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB

func InitDB() (err error) {
	// DSN(Data Source Name) - 数据库连接数据源
	dsn := "root:19960708echokim@tcp(127.0.0.1:3306)/entrytask?charset=utf8&parseTime=True"
	Db, err = sql.Open("mysql", dsn)
	if err != nil {
		fmt.Printf("DSN : %s Format failed\n %v \n", dsn, err)
		return err
	}
	// 尝试与数据库建立连接（校验DSN是否正确）
	err = Db.Ping()
	if err != nil {
		fmt.Printf("Connection %s Failed,\n%v \n", dsn, err)
		return err
	}
	// 设置与数据库建立连接的最大数目
	Db.SetMaxOpenConns(1024)
	// 设置连接池中的最大闲置连接数，0 表示不会保留闲置。
	Db.SetMaxIdleConns(0)
	fmt.Println("Mysql connection success!")
	return nil
}

func MysqlClose() {
	Db.Close()
}

func FindUserByName(name string) (user protocal.User, err error) {
	//var user protocal.User
	sqlStr := "select * from user where username = ?"
	row := Db.QueryRow(sqlStr, name)
	err = row.Scan(&user.Uid, &user.Username, &user.Password, &user.Nickname, &user.Picture)
	return user, err
}

//func LoginValid(req protocal.LoginReq) (user protocal.User, err error) {
//	sqlStr := "select * from user where username = ? and password = ?"
//	rows, err := Db.Query(sqlStr, req.Username, req.Password)
//	if err != nil {
//		fmt.Printf("query failed, err:%v\n", err)
//		return user, err
//	}
//	// 非常重要：关闭rows释放持有的数据库链接 [否则将一直占有连接池资源导致后续无法正常连接]
//	defer rows.Close()
//
//	//var user protocal.User
//	// 读取结果集中的数据
//	if rows.Next() {
//		err := rows.Scan(&user.Uid, &user.Username, &user.Password, &user.Nickname, &user.Picture)
//		if err != nil {
//			fmt.Printf("scan failed, err:%v\n", err)
//			return user, err
//		}
//		fmt.Printf("id:%d name:%s pwd:%s nickname:%s pic:%s \n", &user.Uid, &user.Username, &user.Password, &user.Nickname, &user.Picture)
//	}
//	return user, nil
//}

func UpdateNick(username string, newnick string) (user protocal.User, err error) {
	sqlStr := "update user set nickname = ? where username = ?"
	_, err = Db.Exec(sqlStr, newnick, username)
	if err != nil {
		fmt.Printf("update failed, err:%v\n", err)
		return user, err
	}
	return FindUserByName(username)
}

func UpdatePic(username string, newpic string) (user protocal.User, err error) {
	sqlStr := "update user set picture = ? where username = ?"
	_, err = Db.Exec(sqlStr, newpic, username)
	if err != nil {
		fmt.Printf("update failed, err:%v\n", err)
		return user, err
	}
	return FindUserByName(username)
}
