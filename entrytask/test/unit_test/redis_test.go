package unit

import (
	"crypto/md5"
	"entrytask/dao"
	"entrytask/src/protocal"
	"fmt"
	"strconv"
	"testing"
	"time"
)

func InitRe(t *testing.T) {
	err := dao.InitRedis()
	if err != nil {
		t.Log(err)
		return
	}
}

func TestSetUser(t *testing.T) {
	InitRe(t)
	user := protocal.User{
		Uid:      2,
		Username: "newuser",
		Password: "userpwd",
		Nickname: "nicknew",
		Picture:  "pic/pic1.jpeg",
	}
	err := dao.SetUser(user)
	if err != nil {
		t.Errorf("redis set user error, error: %s", err.Error())
		t.FailNow()
	}
}

func TestGetUser(t *testing.T) {
	InitRe(t)
	username := "newuser"
	user, err := dao.GetUser(username)
	if err != nil {
		t.Errorf("redis get user error, error: %s", err.Error())
		t.FailNow()
	}
	if user == (protocal.User{}) {
		t.Errorf("not get userinfo from redis")
		t.FailNow()
	}
	expect_res := protocal.User{
		Uid:      2,
		Username: "newuser",
		Password: "userpwd",
		Nickname: "nicknew",
		Picture:  "pic/pic1.jpeg",
	}
	if user != expect_res {
		t.Error("expected:", expect_res, "got:", user)
	} else {
		fmt.Println(user)
	}
}

func TestDelUser(t *testing.T) {
	InitRe(t)
	username := "newuser"
	err := dao.DelUser(username)
	if err != nil {
		t.Errorf("redis delete user error, error: %s", err.Error())
		t.FailNow()
	}
	fmt.Println("delete user from redis success")
}

func TestSetToken(t *testing.T) {
	InitRe(t)
	username := "newuser"
	srcstr := strconv.FormatInt(time.Now().Unix(), 10) + username
	h := md5.New()
	h.Write([]byte(srcstr))
	//io.WriteString(h, )
	expect_str := fmt.Sprintf("%x", h.Sum(nil))
	token, err := dao.SetToken(username)
	if err != nil {
		t.Errorf("redis set token error, error: %s", err.Error())
		t.FailNow()
	}
	if token == "" {
		t.Errorf("redis set token success, while get it failed")
		t.FailNow()
	}
	if token != expect_str {
		t.Error("expected:", expect_str, "got:", token)
	} else {
		fmt.Println(token)
	}
}

//func GetToken(username string) (token string, err error)
func TestGetToken(t *testing.T) {
	InitRe(t)
	username := "newuser"
	token, err := dao.GetToken(username)
	if err != nil {
		t.Errorf("redis get token error, error: %s", err.Error())
		t.FailNow()
	}
	if token == "" {
		t.Errorf("not get token from redis")
		t.FailNow()
	}
	fmt.Println(token)
}

func TestDeleteToken(t *testing.T) {
	InitRe(t)
	username := "newuser"
	err := dao.DeleteToken(username)
	if err != nil {
		t.Errorf("redis delete token error, error: %s", err.Error())
		t.FailNow()
	}
	fmt.Println("delete token from redis success")
}
