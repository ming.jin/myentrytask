package unit_test

import (
	"entrytask/dao"
	"entrytask/src/protocal"
	"fmt"
	"testing"
)

func InitSql(t *testing.T) {
	err := dao.InitDB()
	if err != nil {
		t.Log(err)
		return
	}
}

func TestFindUserByName(t *testing.T) {
	InitSql(t)
	user, err := dao.FindUserByName("jinming")
	if err != nil {
		t.Errorf("get user by username error, error: %s", err.Error())
		t.FailNow()
	}
	if user == (protocal.User{}) {
		t.Errorf("find error while using right usename")
		t.FailNow()
	}
	expect_res := protocal.User{
		Uid:      3,
		Username: "jinming",
		Password: "123456",
		Nickname: "我又改名字啦",
		Picture:  "pic/pic3.jpeg",
	}
	if user != expect_res {
		t.Error("expected:", expect_res, "got:", user)
	} else {
		fmt.Println(user)
	}
}

func TestUpdateNick(t *testing.T) {
	InitSql(t)
	username := "jinming"
	newnick := "我再改个名字"
	user, err := dao.UpdateNick(username, newnick)
	if err != nil {
		t.Errorf("update nickname error, error: %s", err.Error())
		t.FailNow()
	}
	if user == (protocal.User{}) {
		t.Errorf("update nickname success while get new userinfo failed")
		t.FailNow()
	}
	expect_res := protocal.User{
		Uid:      3,
		Username: "jinming",
		Password: "123456",
		Nickname: "我再改个名字",
		Picture:  "pic/pic2.jpeg",
	}
	if user != expect_res {
		t.Error("expected:", expect_res, "got:", user)
	} else {
		fmt.Println(user)
	}
}

//func UpdatePic(username string, newpic string) (user protocal.User, err error) {
func TestUpdatePic(t *testing.T) {
	InitSql(t)
	username := "jinming"
	newpic := "pic/pic1.jpeg"
	user, err := dao.UpdatePic(username, newpic)
	if err != nil {
		t.Errorf("update picture error, error: %s", err.Error())
		t.FailNow()
	}
	if user == (protocal.User{}) {
		t.Errorf("update picture success while get new userinfo failed")
		t.FailNow()
	}
	expect_res := protocal.User{
		Uid:      3,
		Username: "jinming",
		Password: "123456",
		Nickname: "我再改个名字",
		Picture:  "pic/pic1.jpeg",
	}
	if user != expect_res {
		t.Error("expected:", expect_res, "got:", user)
	} else {
		fmt.Println(user)
	}
}
