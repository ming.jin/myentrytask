package unit

import (
	"encoding/gob"
	dao2 "entrytask/dao"
	"entrytask/service"
	"entrytask/src/protocal"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"testing"
)

func InitTcpsvr(t *testing.T) {
	gob.Register(protocal.LoginReq{})
	gob.Register(protocal.LoginRes{})
	gob.Register(protocal.ShowInfoReq{})
	gob.Register(protocal.ShowInfoRes{})
	gob.Register(protocal.AlterNickReq{})
	gob.Register(protocal.AlterNickRes{})
	gob.Register(protocal.AlterPicReq{})
	gob.Register(protocal.AlterpicRes{})
	gob.Register(protocal.LogoutReq{})
	gob.Register(protocal.User{})
	close()
	err := dao2.InitDB()
	if err != nil {
		t.Log(err)
		return
	}
	err = dao2.InitRedis()
	if err != nil {
		t.Log(err)
		return
	}
}

func close() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGKILL)
	go func() {
		<-c
		dao2.MysqlClose()
		dao2.RedisClose()
		fmt.Println("close connection...")
		os.Exit(0)
	}()
}

func TestAuth(t *testing.T) {
	InitTcpsvr(t)
	username := "newuser"
	tokenpara := "da0a7d0aa62dcf6e97b12eb1ba52a6e8"
	msg, code := service.Auth(username, tokenpara)
	t.Log(code, msg)
}

func TestLogin(t *testing.T) {
	InitTcpsvr(t)
	req := protocal.LoginReq{
		Username: "newuser",
		Password: "userpwd",
	}
	resp, msg, code := service.Login(req)
	t.Log(code, msg, resp)
}

func TestGetUserInfo(t *testing.T) {
	InitTcpsvr(t)
	req := protocal.ShowInfoReq{
		Token:    "b085fa89bc1a00c6c7ac3344d01e13de",
		Username: "newuser",
	}
	resp, msg, code := service.GetUserInfo(req)
	t.Log(code, msg, resp)
}

func TestAlterNickname(t *testing.T) {
	InitTcpsvr(t)
	req := protocal.AlterNickReq{
		Token:       "12f1544045c04675122a58548c03c6e4",
		Username:    "newuser",
		NewNickname: "mynewnick",
	}
	resp, msg, code := service.AlterNickname(req)
	t.Log(code, msg, resp)
}

func TestAlterPic(t *testing.T) {
	InitTcpsvr(t)
	req := protocal.AlterPicReq{
		Token:    "9fe2987d042cd3d0310863ecc73ec73a",
		Username: "newuser",
		Newpic:   "pic/pic1.jpeg",
	}
	resp, msg, code := service.AlterPic(req)
	t.Log(code, msg, resp)
}
