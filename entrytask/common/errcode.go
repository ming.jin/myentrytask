package common

const AuthSuccess int = 999
const Correct int = 1000
const MysqlOpError int = 1001
const RedisOpError int = 1002
const PwdError int = 1003
const NoUserError int = 1004
const SaveTokenError int = 1005
const LockParaError int = 1006
const AuthError int = 1007
const NoCookieError = 1008
const UpdateNickError = 1009
const UpdatePicError = 1009
const DeleteTokenError = 1009
const NickSameError = 1009
