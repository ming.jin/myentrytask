package common

import (
	"encoding/json"
	dao2 "entrytask/dao"
	"fmt"
	"log"
	"net/http"
	"os"
	"syscall"
)

const (
	DEFAULT_ERROR_CODE    int32 = -1
	PARAMETER_ERROR_CODE  int32 = -2
	CREAT_FILE_ERROR      int32 = -3
	UPLOADFILE_ERROR_CODE int32 = -4
)

func JsonResp(objResp http.ResponseWriter, data interface{}, msg string, code int) {
	objResp.Header().Set("Content-type", "application/json")
	objResp.Header().Set("Access-Control-Allow-Origin", "*")
	respMap := map[string]interface{}{"data": data, "msg": msg, "code": code}
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	fmt.Println(string(respJson))
	objResp.Write(respJson)
}

func ErrJsonResp(objResp http.ResponseWriter, code int, msg string) {
	objResp.Header().Set("Content-type", "application/json")
	objResp.Header().Set("Access-Control-Allow-Origin", "*")
	respMap := map[string]interface{}{"data": "", "msg": msg, "code": code}
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	fmt.Println(string(respJson))
	objResp.Write(respJson)
}

func WaitElegantExit(c chan os.Signal) {
	for i := range c {
		switch i {
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
			// 这里做一些清理操作或者输出相关说明，比如 断开数据库连接
			dao2.MysqlClose()
			dao2.RedisClose()
			fmt.Println("receive exit signal ", i.String(), ",exit...")
			os.Exit(0)
		}
	}
}
